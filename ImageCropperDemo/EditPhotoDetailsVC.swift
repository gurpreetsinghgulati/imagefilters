//
//  EditPhotoDetailsVC.swift
//  FireBaseDemo
//
//  Created by Gurpreet Gulati on 21/05/20.
//  Copyright © 2020 Gurpreet Gulati. All rights reserved.
//

import UIKit
import CoreImage
//import AlamofireImage


class EditPhotoDetailsVC: UIViewController {

    @IBOutlet weak var mContrastValueLbl: UILabel!
    @IBOutlet weak var mBrightnessValueLbl: UILabel!
    @IBOutlet weak var mSaturationValueLbl: UILabel!
    
    @IBOutlet weak var mContrastSliderOutl: UISlider!
    @IBOutlet weak var mBrightnessSliderOutl: UISlider!
    @IBOutlet weak var mSaturationSliderOutl: UISlider!
    
    @IBOutlet weak var mimageView: UIImageView!
    
    var aCIImage = CIImage();
    var context = CIContext();
    var myselectedImage : UIImage!

    
    
    var contrastSliderValue = Float()
    var brighntessSliderValue = Float()
    var saturationSliderValue = Float()

   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contrastSliderValue = self.mContrastSliderOutl.value
        brighntessSliderValue = self.mBrightnessSliderOutl.value
        saturationSliderValue = self.mSaturationSliderOutl.value
        
        self.mimageView.image = myselectedImage
        let aUIImage = mimageView.image

        //var aCGImage = aUIImage?.cgImage
        aCIImage =    CIImage(image: aUIImage!)! //CIImage(cgImage: aCGImage!)

        context = CIContext(options: nil);

        


        
    }
    
    

    @IBAction func mClearAllTapped(_ sender: UIButton) {
        
        self.mContrastSliderOutl.value = 0
        self.mBrightnessSliderOutl.value = 0
        self.mSaturationSliderOutl.value = 0
        self.contrastSliderValue = Float()
        self.brighntessSliderValue = Float()
        self.saturationSliderValue = Float()
        self.mimageView.image = self.myselectedImage
        self.mContrastValueLbl.text = ""
        self.mBrightnessValueLbl.text = ""
        self.mSaturationValueLbl.text = ""
        
    }
    
    
    @IBAction func slider(_ sender: UISlider) {

        if sender.tag == 0 {
            if sender.value == 0 {
                let generator = UIImpactFeedbackGenerator(style: .heavy)
                      generator.impactOccurred()
            }
      
            
            
            self.contrastSliderValue = sender.value
            self.mContrastValueLbl.text = String(describing:sender.value)
            self.applyFilter()
        }

        else if sender.tag == 1 {
            let value = sender.value
            self.brighntessSliderValue = value
            self.mBrightnessValueLbl.text = String(describing:NSNumber(value:sender.value))
            self.applyFilter()
        }
            
            else if sender.tag == 2 {
            self.saturationSliderValue = sender.value
            self.mSaturationValueLbl.text =  String(describing: sender.value)
            self.applyFilter()
                
        }

        else {
            return
        }
        
       

        }
        
    

    func applyFilter() {
        let newCIImage = self.aCIImage.applyingFilter( "CIColorControls", parameters: [kCIInputBrightnessKey:  self.brighntessSliderValue]).applyingFilter( "CIColorControls", parameters: [kCIInputContrastKey: 1 + self.contrastSliderValue]).applyingFilter( "CIColorControls", parameters: [kCIInputSaturationKey: 1 + self.saturationSliderValue])
            
               let newUIImage = UIImage(ciImage: newCIImage)
               mimageView.image = newUIImage
    }
    
    
    
}
    




 
