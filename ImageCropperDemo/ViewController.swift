//
//  ViewController.swift
//  ImageCropperDemo
//
//  Created by Gurpreet Gulati on 25/05/20.
//  Copyright © 2020 Gurpreet Gulati. All rights reserved.
//

import UIKit
import CropViewController


class ViewController: UIViewController {

    var isImageSelected = false

    @IBOutlet weak var mImageVIew: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }


    @IBAction func mSelectImageTapped(_ sender: UIButton) {
        self.addImage()
        
        
    }
    
    
    @IBAction func mEditBtnTapped(_ sender: UIButton) {
        if isImageSelected {
            let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "EditPhotoDetailsVC") as! EditPhotoDetailsVC
            destinationVC.myselectedImage = self.mImageVIew.image
            
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
        
        else {
            let alert = UIAlertController(title: "Alert", message: "Please Select an Imaage", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
            
        }
    }
    
  
}



extension ViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func addImage(){
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(myPickerController, animated: true, completion: nil)
            
        })
        
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    

    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("abc")
        guard let originalImage = info[.originalImage] as? UIImage else { return }
        
        self.dismiss(animated: false, completion: { [weak self] in
            
          self?.moveToImageCropper(image: originalImage)
        })
    }
    
}




extension ViewController : CropViewControllerDelegate {
    
    private func moveToImageCropper(image: UIImage) {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.delegate = self
        cropController.aspectRatioPickerButtonHidden = false
        cropController.aspectRatioLockEnabled = true
        cropController.aspectRatioPreset = .presetCustom
        self.present(cropController, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.mImageVIew.contentMode = .scaleAspectFill
        self.mImageVIew.image = image
        self.isImageSelected = true
        
   
        
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}



